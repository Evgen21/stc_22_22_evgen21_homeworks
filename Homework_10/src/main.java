import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

       String str = "Hello Hello bye Hello bye Inno";
        System.out.println(splitCount(str));
    }

    public static Map<String, Integer> splitCount(String str) {
        int count = 0;
        Map<String, Integer> repeat = new HashMap<>();
        String[] str1 = str.split(" ");
        for (int i = 0; i < str1.length; ++i) {
            String item = str1[i];

            if (repeat.containsKey(item))
                repeat.put(item, repeat.get(item) + 1);
            else
                repeat.put(item, 1);
        }

        return repeat;
    }
}