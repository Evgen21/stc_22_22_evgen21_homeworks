import java.util.List;

public interface CarsRepository {

    public List<String> AllNumbersCarBlackAndNullMileage();
    public Long countOfUniqueModelPrice(Integer lower, Integer moreThan);
    public String colorAutoMinPrice();
    public Double averageCamryPrice();

}
