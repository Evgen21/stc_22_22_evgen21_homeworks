public class Car {
    public String number;
    public String model;
    public String color;
    public Integer carMillege;
    public Integer price;

    public Car(String number, String model, String color, Integer carMillege, Integer price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.carMillege = carMillege;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getCarMillege() {
        return carMillege;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", carMillege=" + carMillege +
                ", price=" + price +
                '}';
    }
}
