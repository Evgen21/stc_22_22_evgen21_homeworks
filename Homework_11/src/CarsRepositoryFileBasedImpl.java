import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.awt.Color.black;

public class CarsRepositoryFileBasedImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }


    private static final Function<Car, String> carToStringMapper = car ->
            car.getNumber() + "|" + car.getModel() + "|" + car.getColor() + "|" +
                    car.getCarMillege() + "|" + car.getPrice();

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer carMillege = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);

        return new Car(number,model,color,carMillege,price);
    };


    @Override
    public List<String> AllNumbersCarBlackAndNullMileage() {
        try {
             return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car-> car.getColor().equals("Black"))
                    .filter(car -> car.getCarMillege()==0)
                     .map(Car::getNumber).collect(Collectors.toList());


        }catch (IOException e){
            throw new RuntimeException("ошибка работы с файлом!!!");
        }
    }

    @Override
    public Long countOfUniqueModelPrice(Integer lower, Integer moreThan) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car->car.getPrice()>=lower)
                    .filter(car -> car.getPrice()<=moreThan)
                    .count();
        }catch (IOException e){
            throw new RuntimeException("ошибка работы с файлом!!!");
        }

    }

    @Override
        public String colorAutoMinPrice () {
            try {
                return new BufferedReader(new FileReader(fileName))
                        .lines()
                        .map(stringToCarMapper)
                        .min(Comparator.comparingInt(Car::getPrice))
                        .get().getColor();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Double averageCamryPrice () {
            try {
                return new BufferedReader(new FileReader(fileName))
                        .lines()
                        .map(stringToCarMapper)
                        .mapToInt(Car::getPrice)
                        .average().
                        getAsDouble();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
