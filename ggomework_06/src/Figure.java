import java.sql.SQLOutput;

public class Figure {
    protected double x;
    protected double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double mouve(double toX, double toY){
       setY(toY);
       setX(toX);
        System.out.println("Фигура сдвинулась, новые координаты:"+getX()+" "+getY());
        return (getX() + getY());
    }
}
