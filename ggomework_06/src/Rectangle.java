public class Rectangle extends Figure {

    protected double sideOfRectangle;
    protected double anoserSideOfRectangle;

    public Rectangle(double x, double y, double sideOfRectangle, double anoserSideOfRectangle) {
        super(x, y);
        this.sideOfRectangle = sideOfRectangle;
        this.anoserSideOfRectangle = anoserSideOfRectangle;
    }

    public double getSideOfRectangle() {
        return sideOfRectangle;
    }

    public double getAnoserSideOfRectangle() {
        return anoserSideOfRectangle;
    }

    public double getOfPerimetr(){
        double i =  (sideOfRectangle+anoserSideOfRectangle)*2;
        System.out.println("Периметр прямоугольника="+i);
        return i;
    }
    public double  getSquareOfRectangle(){
        double j = sideOfRectangle*anoserSideOfRectangle;
        System.out.println("Площадь прямоугольника="+j);
        return j;
    }
}
