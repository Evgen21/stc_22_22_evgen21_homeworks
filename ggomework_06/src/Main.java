public class Main {
    public static void main(String[] args) {
        Figure figure = new Figure(12,12);

        figure.mouve(20,20);

        Circle circle = new Circle(13,13,5);
        circle.getOfPerimetr();
        circle.getSquareOfCircle();
        circle.mouve(21,21);

        Sqware sqware = new Sqware(14,14,6);
        sqware.getSquareOfSguare();
        sqware.getOfPerimetr();
        sqware.mouve(22,22);

        Rectangle rectangle = new Rectangle(15,15,5,6);
        rectangle.getSquareOfRectangle();
        rectangle.getOfPerimetr();
        rectangle.mouve(23,23);

        Ellipse ellipse = new Ellipse(16,16,5,6);
        ellipse.getSquareOfEllipse();
        ellipse.getOfPerimetr();
        ellipse.mouve(24,42);
    }
}
