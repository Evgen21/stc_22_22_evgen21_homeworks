import static java.lang.Math.sqrt;

public class Ellipse extends Figure{
    protected double bigRadius;
    protected double smallRadius;

    public Ellipse(double x, double y, double bigRadius, double smallRadius) {
        super(x, y);
        this.bigRadius = bigRadius;
        this.smallRadius = smallRadius;
    }

    public double getBigRadius() {
        return bigRadius;
    }

    public double getSmallRadius() {
        return smallRadius;
    }

    public double getOfPerimetr(){
         double i =2*3.14*sqrt( (bigRadius + smallRadius) / 2 );
        System.out.println("Периметр эллипса="+i);
         return i;
    }

    public double getSquareOfEllipse(){
        double j = 3.14*bigRadius*smallRadius;
        System.out.println("Площадь эллипса="+j);
        return j;
    }

}
