import org.w3c.dom.ls.LSOutput;

public class Sqware extends Figure{
    protected double sideOfSqware;

    public Sqware(double x, double y, double sideOfSqware) {
        super(x, y);
        this.sideOfSqware = sideOfSqware;
    }
    public double getOfPerimetr(){
        double i = sideOfSqware*4;
        System.out.println("Периметр квадрата="+i);
        return i;
    }
    public double  getSquareOfSguare(){
        double j = sideOfSqware*sideOfSqware;
        System.out.println("Площадь квадрата=" +j);
        return j;
    }
}
