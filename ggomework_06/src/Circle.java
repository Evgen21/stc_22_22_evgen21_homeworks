


public class Circle extends Figure {
    private final double radius;

    public Circle(double x, double y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getOfPerimetr() {
        double j = 2 * 3.14 * radius;
        System.out.println("Периметр круга=" + j);
        return j;
    }

    public double getSquareOfCircle() {
        double i = 3.14 * (radius * radius);
        System.out.println("Площадь круга=" + i);
        return i;
    }
}
