import java.util.Scanner;

public class Main {

    public class Arrays {

        public static int localMin(int[] arr) {
            int count = 0;

            for (int i = 1; i < arr.length - 1; i++) {
                if (arr[i - 1] > arr[i] && arr[i] < arr[i + 1]) {
                    count++;
                }
            }
            if (arr[0] < arr[1]) {
                count++;
            }
            if (arr[arr.length - 1] < arr[arr.length - 2]) {
                count++;
            }
            return count;
        }


        public static void main(String[] args) {
            int[] arr = new int[10];
            for (int i = 0; i < arr.length - 1; i++) {
                arr[i] = 20 + (int) (Math.random() * 40);
                System.out.println(java.util.Arrays.toString(arr));
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("введите размер массива");
            int arrLenght = scanner.nextInt();
            System.out.println("введите эл-ты массива");
            for (int i = 0; i < arrLenght; i++) {
                arr[i] = scanner.nextInt();
            }
            System.out.println(localMin(arr));

        }
    }

}
