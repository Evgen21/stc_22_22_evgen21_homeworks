

public class Main {

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }

    public static void main(String[] args) {


        Task a1 = new EvenNumbersPrintTask(1, 10);
        Task a2 = new OddNumbersPrintTask(3, 25);
        Task a3 = new EvenNumbersPrintTask(4, 20);
        Task a4 = new OddNumbersPrintTask(5,29);

        Task[] tasks = new Task[]{a1, a2, a3, a4};
        completeAllTasks(tasks);


    }
}
