public class Bancomat {

    private final static int maxValueOfCashInBancomat = 10_000;
    private static final int maxSumAllowed = 10_000;
    int sumOfLast;
    int countOfOperation;

    {
        countOfOperation = 0;
    }

    public Bancomat(int sumOfLast, int countOfOperation) throws IllegalArgumentException {
        if (sumOfLast>maxValueOfCashInBancomat)  {
            throw  new IllegalArgumentException("Слишом много денег.Все не влезают");
        }

        this.sumOfLast = sumOfLast;
        this.countOfOperation = countOfOperation;
    }

    public int getMaxSumAllowed() {
        return maxSumAllowed;
    }

    public int getSumOfLast() {
        System.out.println("Осталось"+sumOfLast+" денег");
        return sumOfLast;
    }

    public void setSumOfLast(int sumOfLast) {
        this.sumOfLast = sumOfLast;
    }

    public int getMaxValueOfCashInBancomat() {
        return maxValueOfCashInBancomat;
    }


    public int getCountOfOperation() {
        System.out.println("Количество операций="+countOfOperation);
        return countOfOperation;
    }

    public int giveCash(int sumOfAllowedCash) {
        countOfOperation += 1;
        if (sumOfAllowedCash <= maxSumAllowed || sumOfAllowedCash <= sumOfLast) {
            System.out.println("Деньги выданы!"+sumOfAllowedCash);
            setSumOfLast(sumOfLast-sumOfAllowedCash);
            return sumOfAllowedCash;
        } else {
            System.out.println("деньги закончились!");
            return sumOfLast;
        }
    }


    public int setDepositMoney(int sumOfDepositCash) {
        countOfOperation += 1;
        if (sumOfDepositCash + sumOfLast > maxValueOfCashInBancomat) {
            System.out.println("Банкомат переполнен!");
            int evrethingThatFit = maxValueOfCashInBancomat-(sumOfLast + sumOfDepositCash);
            return evrethingThatFit;
        } else
            System.out.println("Деньги внесены!");
        setSumOfLast(sumOfLast+sumOfDepositCash);
            return sumOfDepositCash;

    }

}


