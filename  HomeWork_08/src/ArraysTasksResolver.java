import java.util.Arrays;

public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to){

           int result =  task.resolve(array,from,to);

        System.out.println(Arrays.toString(array) +" from "+from+" to "+to+" result "+result);

    }
}
