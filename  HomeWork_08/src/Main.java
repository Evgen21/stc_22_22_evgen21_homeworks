public class Main {
    static int[] array = new int[]{12, 62, 4, 2, 100, 40, 56};
    static int from = 1;
    static int to = 3;

    public static void main(String[] args) {

        ArrayTask task = (array, from, to) -> {
            if (from >= to) {
                return -1;
            }
            int result = 0;
            for (int i = from - 1; i <= to - 1; i++) {
                result += array[i];
            }
            return result;
        };
        ArraysTasksResolver.resolveTask(array, task, from, to);
        ArrayTask task1 = (array, from, to) -> {
            int max = array[from-1];
            for (int i = from-1; i <= to-1; i++){
                                if (array[i]>max){
                                    max =array[i];
                                }
            }
            int result = 0;
            while (max!=0){
                  result +=(max%10);
                 max/=10;
            }
                return result;
        };
        ArraysTasksResolver.resolveTask(array,task1,from,to);

    }

}
