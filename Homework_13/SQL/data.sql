insert into driver (first_name, last_name, fone_number, experience, age, have_permit_conduit, category_of_permit,
                    retying)
values ('Vasa', 'Pupckin', 887654321, 5, 20, true, 'b', 1);
insert into driver (first_name, last_name, fone_number, experience, age, have_permit_conduit, category_of_permit,
                    retying)
values ('Kolya', 'NePupckin', 87654321, 6, 22, true, 'a', 2);
insert into driver (first_name, last_name, fone_number, experience, age, have_permit_conduit, category_of_permit,
                    retying)
values ('Petya', 'VoobsheNePupckin', 8654321, 7, 23, true, 'c', 3);
insert into driver (first_name, last_name, fone_number, experience, age, have_permit_conduit, category_of_permit,
                    retying)
values ('Igor', 'Lucky', 8554321, 8, 34, true, 'd', 4);
insert into driver (first_name, last_name, fone_number, experience, age, have_permit_conduit, category_of_permit,
                    retying)
values ('Evgeniy', 'Gamaley', 89734997, 3, 38, false, 'null', 5);


insert into auto(model, color, number, id_of_proprietary)
VALUES ('Lada', 'black', 'oo111oo', 1);
insert into auto (model, color, number, id_of_proprietary)
values ('Shkoda', 'white', 'o222oo', 2);
insert into auto(model, color, number, id_of_proprietary)
values ('mazda', 'green', 'o333oo', 3);
insert into auto (model, color, number, id_of_proprietary)
values ('toyota', 'green', 'o444oo', 4);
insert into auto(model, color, number, id_of_proprietary)
values ('ford', 'blue', 'o555oo', 5);



insert into trip (driver_id, auto_id, data_of_trip)
values (1, 1, '2022-10-1'),
       (2, 2, '2021-9-2'),
       (3, 3, '2022-9-2'),
       (4, 4, '2020-8-3'),
       (5, 5, '2019-7-4');
