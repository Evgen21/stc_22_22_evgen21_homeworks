drop table if exists driver cascade ;
drop table if exists  auto cascade;
drop table if exists trip cascade;


create table driver
(
    id                  bigserial primary key,
    first_name          char(20),
    last_name           char(20),
    fone_number         integer,
    experience          integer,
    age                 integer check (age >= 18 and age <= 120) not null,
    have_permit_conduit bool default false,
    category_of_permit  char(10),
    retying             integer,
    auto_id             bigserial
-- ,foreign key (auto_id) references auto(id)
);


create table auto
(
    id                bigserial primary key,
    model             char(20),
    color             char(20),
    number            char(10),
    id_of_proprietary integer,
    driver_id         bigserial,
    foreign key (driver_id) references driver (id)
);
create table trip
(
    id           bigserial primary key,
    driver_id    bigserial,
    auto_id      bigserial,
    data_of_trip date,
    foreign key (driver_id) references driver (id),
    foreign key (auto_id) references auto (id)
);

