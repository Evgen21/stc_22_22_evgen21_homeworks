package product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ProductRepositoryBaseimpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryBaseimpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, name, price, count);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getName() + "|" + product.getPrice() + "|" + product.getCount() + "\n";

    @Override
    public Product findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))

                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> Objects.equals(product.getId(), id)).findFirst().get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void update(Product product, Integer id) {
        try {
            List<Product> listProducts = new ArrayList<>(new BufferedReader(new FileReader(fileName))
                    .lines().map(stringToProductMapper).toList());
            for (int i = 0; i < listProducts.size(); i++) {
                if (listProducts.get(i).getId().equals(id)) {
                    listProducts.set(i, product);
                }
            }
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName))) {
                for (var p : listProducts) {
                    bufferedWriter.write(productToStringMapper.apply(p));
                }
            }
        } catch (
                IOException e) {
            throw new RuntimeException(e);
        }

    }
}
