package product;

import java.io.IOException;

public interface ProductRepository {
    Product findById(Integer id) throws IOException;
    void update(Product product,Integer id);

}
